[Setting category="Display Text" name="Display Map Name"]
bool isMapNameDisplayed = true;
[Setting category="Display Text" name="Map Name Styling"]
bool isMapNameStyled = true;
[Setting category="Display Text" name="Display Author Name"]
bool isAuthorNameDisplayed = false;
[Setting category="Display Text" name="Rainbow Pronoun Colors"]
bool hasRainbowPronouns = true;

[Setting hidden]
vec2 windowPos = vec2(100, 100);


array<string> neopronouns = {"E/Eir", "Ey/Em", "Fae/Faer", "Ze/Zir", "Ze/Hir", "Sie/Hir", "Xe/Xem", "Ve/Vir", "Ne/Nim", "Per/Per", "Thon", "Ask Me", "No Pronouns"};
string GetPronouns(CGameCtnChallenge@ map) {
    string mapName = map.MapInfo.Name;
    string authorName = map.MapInfo.AuthorNickName;

    int sum = 0;
    for (int c = 0; c < mapName.Length; ++c) {
        sum += mapName[c];
    }
    for (int c = 0; c < authorName.Length; ++c) {
        sum += authorName[c];
    }

    int msum = (sum * 17) % 50;
    if (msum < 17) {
        return "She/Her";
    }
    else if (msum < 33) {
        return "He/Him";
    }
    else if (msum < 40) {
        return "They/Them";
    }
    switch (msum) {
        case 41:
            return "They/He";
        case 42:
            return "He/They";
        case 43:
            return "She/They";
        case 44:
            return "They/She";
        case 45:
            return "He/She";
        case 46:
            return "She/He";
        case 47:
            return "It/Its";
        case 48:
            return "They/It";
        case 49:
            return "Any Pronouns";
        default:
            return neopronouns[(sum * 7) % neopronouns.Length];
    }
}

int ToColorHex(int char) {
    int hex = ( (char * 5) % 12) + 4;
    if (hex < 10) {
        return hex + 48;
    }
    return hex + 55;
}
string GetColorCode(CGameCtnChallenge@ map) {
    string mapName = map.MapInfo.Name;
    string authorName = map.MapInfo.AuthorNickName;

    array<int> rawHex = {0, 0, 0};
    for (int c = 0; c < mapName.Length; ++c) {
        rawHex[c % 2] += mapName[c];
        rawHex[c % 3] += mapName[c];
    }
    for (int c = 0; c < authorName.Length; ++c) {
        rawHex[1 + (c % 2)] += authorName[c];
        rawHex[c % 3] += authorName[c];
    }

    string color = "\\$FFF";
    for (int h = 0; h < 3; ++h) {
        color[2 + h] = ToColorHex(rawHex[h]);
    }
    return color;
}


void Render() {
    if (!UI::IsGameUIVisible()) {
        return;
    }
    
    windowPos = UI::GetWindowPos();
    UI::SetNextWindowPos(int(windowPos.x), int(windowPos.y), UI::Cond::FirstUseEver);

    int windowFlags = UI::WindowFlags::NoTitleBar | UI::WindowFlags::NoCollapse | UI::WindowFlags::AlwaysAutoResize | UI::WindowFlags::NoDocking;
    if (!UI::IsOverlayShown()) {
        windowFlags |= UI::WindowFlags::NoInputs;
    }

    CTrackMania@ app = cast<CTrackMania>(GetApp());
#if TMNEXT||MP4
    CGameCtnChallenge@ map = app.RootMap;
#elif TURBO
    CGameCtnChallenge@ map = app.Challenge;
#endif
    if (map !is null && map.MapInfo.MapUid != "" && app.Editor is null) {
        UI::Begin("Track Pronouns", windowFlags);
        if (isMapNameDisplayed) {
            UI::Text(isMapNameStyled ? ColoredString(map.MapInfo.Name) : StripFormatCodes(map.MapInfo.Name));
        }
        if (isAuthorNameDisplayed) {
            UI::Text("\\$888" + StripFormatCodes(map.MapInfo.AuthorNickName));
        }
        string color = hasRainbowPronouns ? GetColorCode(map) : "";
        UI::Text(color + GetPronouns(map));
        UI::End();
    }
}
